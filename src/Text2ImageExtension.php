<?php

namespace Bolt\Extension\Edwin\Text2Image;

use Bolt\Extension\SimpleExtension;
use Priler\Text2Image\Magic as Text2Image;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require "../vendor/priler/text2image/src/magic.class.php";


/**
 * ExtensionName extension class.
 *
 * @author Your Name <you@example.com>
 */
class Text2ImageExtension extends SimpleExtension
{
    protected function registerFrontendRoutes(ControllerCollection $collection)
    {

        $collection->match('/text2image/{input}/', [$this, 'text2image']);
    }


    /**
     * Handles GET requests on the /example/url route.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function text2image(Request $request, $input)
    {
        $text = new Text2Image($input);
        $width = $request->get('width', 200);

        $text->set_mode('smart');
        $text->add_font('MyFont', './helvetica.ttf');   // must be in de public folder for now..
        $text->font = $text->get_font('MyFont');
        $text->background_color = '#fff'; // custom background color
        $text->text_color = 0;
        $text->text_color = '#000'; // custom text color
        $text->text_size = 12;
        $text->line_height = 15; // custom line height
        $text->width = $width;
        $text->padding = 1; // custom padding
        $text->output();
        die();
    }
}
